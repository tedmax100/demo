#first stage - builder
FROM golang  AS build-env
WORKDIR /go/src/github.com/tedmax100/mahjong
COPY . .

ENV GO111MODULE=on
RUN CGO_ENABLED=0 GOOS=linux go build -o main

#second stage
FROM scratch
WORKDIR /bin
COPY --from=build-env /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=build-env /go/src/github.com/tedmax100/mahjong .
CMD ["./main"]
